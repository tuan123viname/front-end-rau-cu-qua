import React, {useState,useEffect} from 'react';
import './header.css';
import {Cart} from '../index';

export default function Header(props){
    const [widthDevice,setWidthDevice]=useState(window.innerWidth);
    const [marginLeft,setMarginLeft]=useState(-250);
    const [display,setDisplay]=useState('none');
    const [subMenu,setSubMenu]=useState('none');

    //header pc
    useEffect(()=>{
        window.addEventListener('resize',()=>{
            setWidthDevice(window.innerWidth);
        })
    })
    function changeMarginLeft()
    {
        setMarginLeft(0);
        setDisplay('block');
    }
    function closeMenuMobile()
    {
        setDisplay('none');
        setMarginLeft(-250);
    }
    function displaySubMenu()
    {
        if(subMenu==='none')
        {
            setSubMenu('block');
            document.getElementsByClassName('icon-drop-menu')[0].innerHTML='<i class="fa fa-arrow-alt-circle-up"></i>';
        }
        else
        {
            setSubMenu('none');
            document.getElementsByClassName('icon-drop-menu')[0].innerHTML='<i class="fa fa-arrow-alt-circle-down"></i>';
        }
        subMenu==='none' ? setSubMenu('block') : setSubMenu('none');
        
    }
    const headerPC=<section className="header-pc">
        <div className="logo">
            <span>Cfx_Tech</span>
        </div>
        <div className="navigation">
            <div className="nav-left">
                <ul className="menu-pc">
                    <li>
                       <a href="#">Trang chủ</a> 
                    </li>
                    <li>
                       <a href="#">Giới thiệu</a> 
                    </li>
                    <li>
                       <a href="#">Cửa hàng</a> 
                       <ul className="sub-menu-pc">
                           <li><a href="#">Rau củ</a></li>
                           <li><a href="#">Trái cây</a></li>
                           <li><a href="#">Đồ uống</a></li>
                           <li><a href="#">Đồ khô</a></li>
                       </ul>
                    </li>
                    <li>
                       <a href="#">Tham khảo</a> 
                    </li>
                    <li>
                       <a href="#">Liên hệ</a> 
                    </li>
                </ul>
            </div>
            <div className="nav-right">
                 <span><i className="fa fa-search"></i></span>
                 <span><i className="fa fa-user"></i></span>
                 <span><i className="fa fa-cart-arrow-down"></i></span>
               
            </div>
        </div>
        <div className="clearfix"></div>
    </section>;

    //header mobile
     const menuMobile=<div>
     <div className="wrap-menu-mobile" style={{display:display}} onClick={closeMenuMobile}>
         <span><i className="fa fa-times"></i></span>
     </div>
        <ul className="menu-mobile" style={{marginLeft:marginLeft}}>
                <li>
                    <a href="#">Trang chủ</a> 
                </li>
                <li>
                    <a href="#">Giới thiệu</a> 
                </li>
                <li>
                    <a href="#">Cửa hàng</a> 
                    <span class="icon-drop-menu" onClick={displaySubMenu}><i class="fa fa-arrow-alt-circle-down"></i></span>
                    <ul className="sub-menu-mobile" style={{display:subMenu}}>
                        <li>
                            <a href="#">Rau củ</a>
                        </li>
                        <li>
                            <a href="#">Trái cây</a>
                        </li>
                        <li>
                            <a href="#">Đồ uống</a>
                        </li>
                        <li>
                            <a href="#">Đồ khô</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">Tham khảo</a> 
                </li>
                <li>
                    <a href="#">Liên hệ</a> 
                </li>
            </ul>
        </div>
    const headerMobile=<section class="header-mobile">
       <div className="nav-mobile">
           <div className="icon-menu">
            <span onClick={changeMarginLeft}>
                    <i className="fa fa-bars"></i>
            </span>

           </div>
          
            <div className="logo-mobile">
                <span >Cfx_Tech</span>
            </div>
            <div className="nav-mobile-right">
                <span><i className="fa fa-search"></i></span>
                <span><i className="fa fa-cart-arrow-down"></i></span>
                <span><i className="fa fa-user"></i></span>
            </div>
       </div>
       <div className="clearfix"></div>
        {menuMobile}
    </section>

    return widthDevice>800?headerPC : headerMobile;
}