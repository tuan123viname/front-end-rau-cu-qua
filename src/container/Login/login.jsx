import {Login,Header,Footer} from '../../components'
import useGlobal from '../../store';

import React, {useState, useEffect} from 'react';

export const LoginPage = (props) => {
    const [globalState, globalActions] = useGlobal();
    
    useEffect (() => {
        
    })
    const login = async (data) =>{
       await  globalActions.Login.Login(data.username,data.password);
    }
    return (
        <div>
            <Header  />
            <Login login={login} />
            <Footer />
        </div>
    );
}