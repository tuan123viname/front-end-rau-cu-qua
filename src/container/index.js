import {LoginPage} from './Login/login';
import {HomePage} from './Home/home';
import {SignUpPage} from './SignUpPage/signUpPage';
export {
    LoginPage,
    HomePage,
    SignUpPage
}