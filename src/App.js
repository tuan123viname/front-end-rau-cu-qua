import React,{useState, useEffect} from 'react'
import {Header,Carousel,ListItem,Footer,Cart,AdminHeader,Login} from './components';
import {LoginPage, HomePage,SignUpPage} from './container';
import './App.css';
import './public/fontawesome-free/css/all.css';
import './public/fontawesome-free/js/all.js';
import './public/css/reset.css';
import './public/css/grid.css';


import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";

export default function App(props)
{
  
    // const listProduct=[
    //     {
    //         image:img1,
    //         name:'cam',
    //         prince:20000,
    //         number:2
    //     },
    //     {
    //         image:img2,
    //         name:'tao',
    //         prince:40000,
    //         number:3
    //     },
    //     {
    //         image:img4,
    //         name:'buoi',
    //         prince:40000,
    //         number:3
    //     },
    //     {
    //         image:img3,
    //         name:'chanh',
    //         prince:40000,
    //         number:3
    //     },
    //     {
    //         image:img2,
    //         name:'tao',
    //         prince:40000,
    //         number:3
    //     },
    //     {
    //         image:img4,
    //         name:'buoi',
    //         prince:40000,
    //         number:3
    //     },
    //     {
    //         image:img3,
    //         name:'chanh',
    //         prince:40000,
    //         number:3
    //     },
    // ]
    const [listProduct, setListProduct] =  useState (JSON.parse(localStorage.getItem('product')));
    localStorage.setItem('product',JSON.stringify(listProduct))
    
    useEffect(() =>{
        
      
    })
    function removeProduct(index){
        console.log(index);
      
        listProduct.splice(index,1);
       setListProduct([...listProduct]);
       localStorage.setItem('product',JSON.stringify(listProduct));
       return listProduct;
    }
    return (
    <div className="container-fluid">
        <Router>
            <div>
                <Route path='/dang-nhap' component={LoginPage} />
                <Route path exact = '/' component={HomePage} />
                <Route path='/dang-ky' component={SignUpPage} />
            </div>
        </Router>
        {/* <Header product={listProduct} removeProduct={removeProduct} />
        <Carousel ImageList={listImage} />
        <Cart product={listProduct} removeProduct={removeProduct} /> */}
        {/* <ListItem /> */}
        {/* <Footer /> */}
        {/* <AdminHeader/>
        <Login /> */}
    </div>)
}
