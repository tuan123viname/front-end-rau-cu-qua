import React, {useState} from 'react';
import './header.css';
export const Header = (props) => {
    const header=<section className="header-container">
        <div className="header-logo">
            CFX_TECH
        </div>
        <div className="title">DashBoard</div>
        <div className="menu-right">
            <span><i className="fa fa-user"></i></span>
        </div>
        <div className="clearfix"></div>
    </section>
    return header;
}