import React , { useState } from 'react';
import './footer.css';
export const Footer = (props) => {
    const footer=<section className="footer-container">
        <div className="footer-left">
            <ul>
                <li>
                    <strong>CFX_TECH</strong>
                </li>
                <li>
                    ký túc xá khu A, khu phố 6, phường Linh Trung, quận Thủ Đức, tp Hồ Chí Minh
                </li>
                <li>
                    17521230@gm.uit.edu.vn
                </li>
                <li>
                    0967537483
                </li>
            </ul>
        </div>
        <div className="footer-right">
            <div className="menu">
                <ul>
                    <li><strong>MENU</strong></li>
                    <li>Trang chủ</li>
                    <li>Giới thiệu</li>
                    <li>Cửa hàng</li>
                    <li>liên hệ</li>
                </ul>
            </div>
            <div className="product">
                 <ul>
                    <li><strong>SẢN PHẨM</strong></li>
                    <li>Rau củ</li>
                    <li>Trái cây</li>
                    <li>Đồ uống</li>
                    <li>Đồ khô</li>
                </ul>
            </div>
        </div>
        <div className="clear-fix"></div>
        <div className="footer-bottom">
            <i>Bản quyền thuộc về CFX_TECH</i>
        </div>
    </section>
    return footer;
}