import React,{useState} from 'react';
import './sign-up.css';

export const SignUp = (props) => {
    const signUp= <section className="signup-container">
        <div className="form-wrap">
            <div className="form-title">Đăng ký</div>
            <table>
                <tr>
                    <td>Tài khoản: </td>
                    <td><input type="text" /></td>
                </tr>
                <tr>
                    <td>Mật khẩu:</td>
                    <td><input type="password" /></td>
                </tr>
                <tr>
                    <td>Nhập lại mật khẩu: </td>
                    <td><input type="password" /></td>
                
                </tr>
                <tr>
                    
                    <td></td>
                    <td><button className="btn-signup" type="button">Đăng ký</button></td>
                </tr>
               
            </table>
        </div>
    </section>  
    return signUp;
}