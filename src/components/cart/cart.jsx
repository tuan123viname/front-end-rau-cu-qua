import React,{useState, useEffect} from 'react';
import './cart.css';
export const Cart = ( props ) => {
    const [ product , setProduct ] = useState(props.product||{});
    const [ totalPrince, setTotalPrince] = useState (0);
    const countPrince = () => {
        let prince =0;
        product.map( ( item , index ) => {
            prince += item.prince * item.number;
        })
        return prince;
    }
    
    useEffect( () => {
        const prince = countPrince();
        setTotalPrince( prince );
    })

    const cart=<div className="cart-container">
        <ul className="cart">
            {
                product.map(( item, index ) => 
                <li  key={index}>
                    <img src={item.image} alt=""/>
                    <span className="product-info">
                        <div>{item.name}</div>
                        <div >{item.number} x { item.prince}</div>
                    </span>
                    <button  className="btn-remove-product" onClick={() =>setProduct(props.removeProduct({index})) } ><i className="fa fa-times"></i></button>
                    <div className="clear-fix"></div>
            </li>)
            }
            <li> <strong style={{color:'green'}}>Tổng: {totalPrince}đ</strong> </li>
            <li className="btn-view-cart"><button>Xem giỏ hàng</button></li>
            <li className="btn-paid"><button >Thanh toán</button></li>
        </ul>
    </div>
    return cart;
}