import React, {useState,useEffect} from 'react';
import './login.css';
import useGlobal from '../../store';
export const Login = (props) => {
    const [username,setUsername] = useState ('');
    const [password, setPassword] =useState ('');
   
    const [globalState,globalAction]=useGlobal();
    const isLogin=globalState.isLogin;
    const message=globalState.message;
    const handleUsername = (e) =>{
        setUsername(e.target.value);
    }
    const handlePassword = (e) => {
        setPassword(e.target.value);
    }
    const hanldeSubmit = () => {
        
       if(username&&password)
       {
         
            props.login({username:username,password:password});
       }
    }

    const messageLabel=<div className="message">{message}</div>
    const login= <section className="login-container">
        <div className="form-input">
            <div className="form-title">Dang nhap: </div>
            <div className="input"><label htmlFor="">Tai khoan: </label><input type="text" onChange={handleUsername} placeholder="tai khoan..." /></div>
            <div className="input"><label>Mat khau: </label><input type="password" onChange={handlePassword} placeholder="mat khau..." /></div>
            <div className="btn-submit"><button type="button" onClick={hanldeSubmit}>Dang nhap</button></div>
            {messageLabel}
            {isLogin ? <div>da dang nhap</div>: <div>chua dang nhap</div>}
        </div>
       
    </section>
    return login;
}