import React, { useState, useEffect } from 'react';
import './carousel.css';

export const Carousel = ( props ) => {

    const [ imageList , setImageList ]= useState ( props.ImageList || [] );
    const [ marginLeft , setMarginLeft ] = useState( 0 );
    const [ widthCarousel , setWidthCarousel ] = useState( imageList.length*100 );
    const [ dotActive,setDotActive ]= useState(0);

    // setState trong hooks la bat dong bo

    const arrowLeftControl = () => {
        marginLeft === 0 ? setMarginLeft( - (imageList.length) +1 ) : setMarginLeft( marginLeft+1 );
    }

    const arrowRightControl = () => {
       marginLeft === -imageList.length + 1  ? setMarginLeft( 0 ) : setMarginLeft( marginLeft-1 );
    }
    
    const pickSlide = (index) => {
        setMarginLeft( -index );
        setDotActive(index);
    }

    useEffect(()=>{
        setDotActive(-marginLeft);
        let dot = document.getElementsByClassName( 'dot' );
        dot[dotActive].classList.add( 'active' );
        return () => {
            for( let i=0 ; i<dot.length ; i++ )
            {

                dot[i].classList.remove('active');
            }
        }
    })
    
    const createSlideItem = ( img='' , title = '' , content = '' ) => {
        const slideItem =
            <div className = "slide-item" style = {{ backgroundImage:`linear-gradient(to right, rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0)),
            url(${img})` , width: `${100/imageList.length}% `}}>
            <h3 className = "slide-title" > {title} </h3>
            <p className = "slide-content" > {content} </p>
            </div>
        return slideItem;
    }

    const carousel= <section className = "carousel" style = {{marginLeft : marginLeft*100+'%' , width : widthCarousel + '%'}}>
        {
            imageList.map(( item , index ) => {
            return createSlideItem( item.image , item.title , item.content)
            })
        }
    <div className = "clear-fix" ></div>
    <div className = "dot-control" >
        {
            imageList.map( ( item , index ) => 
             <span onClick={()=>{pickSlide(index)}} key={index} className="dot"></span> )
        }
    </div>
    <div className = "arrow-control left" >
        <span onClick = {arrowLeftControl} ><i className = "fa fa-arrow-left" ></i></span>
    </div>
    <div className = "arrow-control right">
        <span onClick = {arrowRightControl} ><i className = "fa fa-arrow-right" ></i></span>
    </div>
    </section>
    return <div className = "carousel-container"> {carousel} </div>;    
}