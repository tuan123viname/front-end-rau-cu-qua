import axios from 'axios';

const apiUrl='http://localhost:9000/login';
axios.defaults.headers.post['Content-Type'] ='application/json;charset=utf-8';
axios.defaults.headers.post['Access-Control-Allow-Origin'] = 'http://localhost:3000';
axios.defaults.headers.post['Content-Type'] ='application/x-www-form-urlencoded';  
export const Login = async (store, username,password) => {
    try {
     const res= await axios.post(apiUrl,{
            email    : username,
            password : password
        });
    const isLogin = Boolean(res.data.token.length);
    const user = {
        token:res.data.token
    }
    window.localStorage.setItem('x-access-token',res.data.token);
    const message=res.data.message;
    store.setState({message});
    store.setState({user});
    store.setState({isLogin});
    }
    catch (err){
        const isLogin=false;
        store.setState({isLogin})
    }
}